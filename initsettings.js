import AsyncStorage from '@react-native-async-storage/async-storage';
import store from './store';
import {setAPIKEY} from './slices/apikeyslice';
import {setRemoteServerPort} from './slices/remoteserver/remoteserverport';
import {setRemoteServerAddr} from './slices/remoteserver/remoteserveraddr';

const loadKeys = async () => {
  let _APIKEY = await AsyncStorage.getItem('APIKEY');
  if (_APIKEY) {
    store.dispatch(setAPIKEY(_APIKEY));
  }
};

const loadRemoveServer = async () => {
  let _addr, _port;
  let addr_promise = AsyncStorage.getItem('REMOTESERVERADDR').then(
    addr => (_addr = addr),
  );
  let port_promise = AsyncStorage.getItem('REMOTESERVERPORT').then(
    port => (_port = port),
  );
  await Promise.all([addr_promise, port_promise]);
  if (_addr) {
      store.dispatch(setRemoteServerAddr(_addr));
  }
  if (_port) {
      store.dispatch(setRemoteServerPort(_port));
  }
};

export default initApp = () => {
  loadKeys();
  loadRemoveServer();
};
