import React from 'react';
import {FlatList, Image} from 'react-native';
import RNFS from 'react-native-fs';
import {ListItem, Avatar, Text, Button, Icon} from 'react-native-elements';
import {createStackNavigator} from '@react-navigation/stack';
import GallerySwiper from 'react-native-gallery-swiper';
import {useFocusEffect} from '@react-navigation/native';
import {VideoPlayer} from './player';
import PlaceHolder from './utility/placeholdertext';

const Stack = createStackNavigator();
const isVideo = filename => {
  let re = /[mkv|mp4|webm]$/i;
  let result = filename.match(re);
  return result;
};
const isMedia = filename => {
  let re = /[jpg|jpeg|png]$/i;
  return filename.match(re) || isVideo(filename);
};
const LocalVideoView = ({navigation, route}) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        style={{flex: 1}}
        name="Videos"
        component={VideoListView}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Player"
        component={VideoPlayer}
        options={({route}) => ({title: route.params.filename})}
      />
      <Stack.Screen
        name="ImageView"
        component={ImageView}
        options={({route}) => ({title: route.params.filename})}
      />
    </Stack.Navigator>
  );
};

const VideoListView = props => {
  const navigation = props.navigation;
  const [files, setFiles] = React.useState([]);
  const getFiles = async () => {
    let files = await RNFS.readDir(RNFS.DocumentDirectoryPath);
    files = files.filter(file => isMedia(file.name));
    files.sort((a, b) => {
      let a_video = isVideo(a.name);
      let b_video = isVideo(b.name);
      if (a_video && !b_video) {
        return 1;
      } else if (b_video && !a_video) {
        return -1;
      }
      return 0;
    });
    setFiles(files);
  };
  React.useEffect(() => {
    getFiles();
  }, [navigation]);

  useFocusEffect(
    React.useCallback(() => {
      getFiles();
      return () => {};
    }, []),
  );

  const renderItem = ({item}) => {
    if (isVideo(item.name)) {
      return (
        <VideoItem item={item} navigation={navigation} onChange={getFiles} />
      );
    } else {
      return (
        <ImageItem item={item} navigation={navigation} onChange={getFiles} />
      );
    }
  };
  if (files.length) {
    return (
      <FlatList
        style={{flex: 1}}
        data={files}
        renderItem={renderItem}
        keyExtractor={item => item.name}
      />
    );
  } else {
    return <PlaceHolder text="Local Folder is Empty." />;
  }
};

const ImageView = ({navigation, route}) => {
  let url = route.params.path;
  let width, height;
  Image.getSize(url, (m_width, m_height) => {
    width = m_width;
    height = m_height;
  });
  return <GallerySwiper images={[{url: url, height: height, width: width}]} />;
};

const VideoItem = ({item, navigation, onChange}) => {
  const switchToPlayer = () => {
    navigation.navigate('Player', {path: item.path});
  };
  return (
    <ListItem.Swipeable
      bottomDivider
      key={item.path}
      onPress={switchToPlayer}
      rightContent={
        <Button
          title="Delete"
          icon={{name: 'delete', color: 'white'}}
          buttonStyle={{minHeight: '100%', backgroundColor: 'red'}}
          onPress={() => {
            RNFS.unlink(item.path);
            onChange();
          }}
        />
      }>
      <Icon name="ondemand-video" />
      <ListItem.Content>
        <ListItem.Title>{item.name}</ListItem.Title>
        <ListItem.Subtitle>
          {`${(item.size / (1024 * 1024)).toFixed(1)} MB`}
        </ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem.Swipeable>
  );
};

const ImageItem = ({item, navigation, onChange}) => {
  const navigateTo = () => {
    navigation.push('ImageView', {
      path: `file://${item.path}`,
    });
  };
  return (
    <ListItem.Swipeable
      key={item.path}
      bottomDivider
      onPress={navigateTo}
      rightContent={
        <Button
          title="Delete"
          icon={{name: 'delete', color: 'white'}}
          buttonStyle={{minHeight: '100%', backgroundColor: 'red'}}
          onPress={() => {
            RNFS.unlink(RNFS.DocumentDirectoryPath + `/${item.name}`);
            onChange();
          }}
        />
      }>
      <Avatar
        size="xlarge"
        rounded
        source={{uri: 'file://' + RNFS.DocumentDirectoryPath + `/${item.name}`}}
      />
      <ListItem.Content>
        <ListItem.Title>{item.name}</ListItem.Title>
        <ListItem.Subtitle>
          {`${(item.size / (1024 * 1024)).toFixed(1)} MB`}
        </ListItem.Subtitle>
      </ListItem.Content>
    </ListItem.Swipeable>
  );
};
export {LocalVideoView};
