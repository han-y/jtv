import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import {FlatList, View, Text} from 'react-native';
import {ListItem, Divider, Icon, Input} from 'react-native-elements';
import Dialog from 'react-native-dialog';
import {useSelector, useDispatch} from 'react-redux';
import {setAPIKEY} from './slices/apikeyslice';
import {setRemoteServerAddr} from './slices/remoteserver/remoteserveraddr';
import {setRemoteServerPort} from './slices/remoteserver/remoteserverport';

const APIKEYSettingDialog = ({visible, onCancel, onComfirm}) => {
  const dispatch = useDispatch();
  const [newKey, setNewKey] = React.useState('');

  let oldKey = useSelector(state => state.APIKEY.value);

  return (
    <Dialog.Container visible={visible}>
      <Dialog.Title>Set API Key</Dialog.Title>
      <Dialog.Description>
        Set the API Key, this will be used as both account name and password.
      </Dialog.Description>
      <Input
        autoFocus={true}
        onChangeText={setNewKey}
        placeholder={oldKey}
      />
      <Dialog.Button
        label="Cancel"
        onPress={() => {
          setNewKey('');
          onCancel();
        }}
      />
      <Dialog.Button
        label="Comfirm"
        onPress={() => {
          if (newKey && newKey !== oldKey) {
            dispatch(setAPIKEY(newKey));
            AsyncStorage.setItem('APIKEY', newKey);
          }
          onComfirm();
        }}
      />
    </Dialog.Container>
  );
};

const RemoteServerDialog = ({visible, onCancel, onComfirm}) => {
  const dispatch = useDispatch();
  const [newAddr, setNewAddr] = React.useState('');
  const [newPort, setNewPort] = React.useState('');
  let oldAddr = useSelector(state => state.RemoteServerAddr.value);
  let oldPort = useSelector(state => state.RemoteServerPort.value);
  return (
    <Dialog.Container visible={visible}>
      <Dialog.Title>Set API Key</Dialog.Title>
      <Dialog.Description>
        Set the API Key, this will be used as both account name and password.
      </Dialog.Description>
      <View>
        <Input
          label="Server Address"
          autoFocus={true}
          onChangeText={setNewAddr}
          placeholder={oldAddr}
        />
        <Input
          label="Server Port"
          autoFocus={true}
          onChangeText={setNewPort}
          placeholder={oldPort.toString()}
        />
      </View>
      <Dialog.Button
        label="Cancel"
        onPress={() => {
          setNewAddr('');
          setNewPort('');
          onCancel();
        }}
      />
      <Dialog.Button
        label="Comfirm"
        onPress={() => {
          if (newAddr && newAddr !== oldAddr) {
            dispatch(setRemoteServerAddr(newAddr));
            AsyncStorage.setItem('REMOTESERVERADDR', newAddr);
          }
          if (newPort && newPort !== oldPort) {
            dispatch(setRemoteServerPort(newPort));
            AsyncStorage.setItem('REMOTESERVERPORT', newPort);
          }
          onComfirm();
        }}
      />
    </Dialog.Container>
  );
};

const SettingPopup = props => {
  let popupType = props.popupType;
  let visible = props.visible;
  switch (popupType) {
    case 'APIKey':
      return (
        <APIKEYSettingDialog
          visible={visible}
          onCancel={props.onCancel}
          onComfirm={props.onComfirm}
        />
      );
    case 'RemoteServer':
      return (
        <RemoteServerDialog
          visible={visible}
          onCancel={props.onCancel}
          onComfirm={props.onComfirm}
        />
      );
    default:
      return null;
  }
};

const SETTINGS_DATA = [
  {
    name: 'API Key',
    popupType: 'APIKey',
  },
  {
    name: 'Remote Server',
    popupType: 'RemoteServer',
  },
];

const Settings = () => {
  const [popupType, setPopupType] = React.useState('');
  const [popupVisible, setPopupVisible] = React.useState(false);

  const RemoteAddr = useSelector(state => state.RemoteServerAddr.value);
  const RemotePort = useSelector(state => state.RemoteServerPort.value);

  const renderListItem = ({item}) => {
    let name = item.name;
    let popupType = item.popupType;
    let subtitle = null;
    let icon = null;
    const handlePress = () => {
      setPopupVisible(true);
      setPopupType(popupType);
    };

    if (popupType === 'RemoteServer') {
      subtitle = <ListItem.Subtitle>{`${RemoteAddr}:${RemotePort}`}</ListItem.Subtitle>;
      icon = <Icon name="cloud" type="ant-design" />
    } else if (popupType === 'APIKey') {
      icon = <Icon name="account" type="material-community" />;
    }
    
    return (
      <ListItem key={name} bottomDivider onPress={handlePress}>
        {icon}
        <ListItem.Content>
          <ListItem.Title style={{fontWeight: 'bold'}}>{name}</ListItem.Title>
          {subtitle}
        </ListItem.Content>
      </ListItem>
    );
  };
  return (
    <View>
      <Divider />
      <FlatList data={SETTINGS_DATA} renderItem={renderListItem}></FlatList>
      <SettingPopup
        popupType={popupType}
        visible={popupVisible}
        onComfirm={() => setPopupVisible(false)}
        onCancel={() => setPopupVisible(false)}
      />
    </View>
  );
};
export {Settings};
