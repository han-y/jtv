import {Card, Text, Button, Icon} from 'react-native-elements';
import {FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PlaceHolder from './utility/placeholdertext';

const Stack = createStackNavigator();

const WebPageCard = ({navigation, url, name}) => {
  const handlePress = () => {
    navigation.navigate('WebView', {url: url});
  };
  return (
    <Card>
      <Card.Title>{name}</Card.Title>
      <Card.Divider />
      <Button
        icon={<Icon name="code" color="#ffffff" />}
        buttonStyle={{
          borderRadius: 0,
          marginLeft: 0,
          marginRight: 0,
          marginBottom: 0,
        }}
        title="前往"
        onPress={handlePress}
      />
    </Card>
  );
};

const WebViewWrapper = ({navigation, route}) => {
  return <WebView style={{flex: 1}} source={{uri: route.params.url}}></WebView>;
};

const HomePageListView = ({navigation, route}) => {
  const APIKEY = useSelector(state => state.APIKEY.value);
  const RemoteServerAddr = useSelector(state => state.RemoteServerAddr.value);
  const RemoteServerPort = useSelector(state => state.RemoteServerPort.value);

  const [websites, setWebsites] = React.useState(null);
  React.useEffect(() => {
    const refreshData = async () => {
      if (APIKEY) {
        try {
          let data = await (
            await fetch(`${RemoteServerAddr}:${RemoteServerPort}/homepage`, {
              headers: {'access-token': APIKEY},
            })
          ).json();
          console.log("fetching from remote", data);
          if (data.tv) {
            let stringified = JSON.stringify(data.tv);
            if (stringified !== JSON.stringify(websites)) {
              setWebsites(data.tv);
              AsyncStorage.setItem('HOMEPAGE_JSON_KEY', stringified);
            }
          }
        } catch (e) {
          console.log(e);
        }
      }
    };
    const getJson = async () => {
      let homepageJsonData = await AsyncStorage.getItem('HOMEPAGE_JSON_KEY');

      let localJson;
      if (homepageJsonData) {
        localJson = JSON.parse(homepageJsonData);
      }
      if (localJson) {
        setWebsites(localJson);
      }
      refreshData();
    };
    getJson();
    setInterval(refreshData, 10000);
  }, []);

  const renderItem = ({item}) => {
    return (
      <WebPageCard url={item.url} name={item.name} navigation={navigation} />
    );
  };
  if (websites) {
    return (
      <FlatList
        style={{flex: 1}}
        data={websites}
        renderItem={renderItem}
        keyExtractor={item => item.name}
      />
    );
  } else if (!APIKEY) {
    return <PlaceHolder text="No API Key Configured" />;
  } else {
    return <PlaceHolder text="Fetching Content..." />;
  }
};

const HomePage = props => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        style={{flex: 1}}
        name="HomePage"
        component={HomePageListView}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="WebView"
        component={WebViewWrapper}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export {HomePage};
