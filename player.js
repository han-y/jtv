import Video from 'react-native-video';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RNFS from 'react-native-fs';
import React from 'react';

const VideoPlayer = ({navigation, route}) => {
  let props = route.params;
  const [player, setPlayer] = React.useState(null);

  const getLastPlayBackTime = async () => {
    let time = 0;
    try {
      if (!player) return;
      time = await AsyncStorage.getItem(`PLAYBACK_TIME_${props.path}`);
    } catch (e) {}
    if (time) {
      let real_time = Number(time);
      if (real_time > 10) {
        player.seek(real_time);
      }
    }
  };

  const saveProgress = async ({currentTime}) => {
    if (currentTime < 15) {
      return;
    }
    try {
      AsyncStorage.setItem(
        `PLAYBACK_TIME_${props.filename}`,
        currentTime.toString(),
      );
    } catch (e) {}
  };

  return (
    <Video
      ref={ref => {
        setPlayer(ref);
      }}
      progressUpdateInterval={10000}
      onProgress={saveProgress}
      onLoad={getLastPlayBackTime}
      resizeMode={'contain'}
      controls={true}
      style={{flex: 1}}
      source={{uri: props.path}}
    />
  );
};

export {VideoPlayer};
