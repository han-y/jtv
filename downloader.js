import RNFS from 'react-native-fs';
import {Button, Icon} from 'react-native-elements';
import React from 'react';
import {PermissionsAndroid, Platform} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';

async function hasAndroidPermission() {
  const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;

  const hasPermission = await PermissionsAndroid.check(permission);
  if (hasPermission) {
    return true;
  }

  const status = await PermissionsAndroid.request(permission);
  return status === 'granted';
}

async function savePicture(url) {
  if (Platform.OS === 'android' && !(await hasAndroidPermission())) {
    return;
  }
  let path = 'file://' + url;
  try {
    await CameraRoll.save(path);
  } catch (e) {
    console.log(e);
  }
}

const download = async props => {
  let filepath = RNFS.DocumentDirectoryPath + `/${props.filename}`;
  const {jobId, promise} = RNFS.downloadFile({
    fromUrl: props.url,
    toFile: filepath,
    progress: props.progressCallback,
    progressDivider: 10,
  });

  return promise.then(result => savePicture(filepath));
};

const isDownloaded = async url => {
  let splited = url.split('/');
  let filename = splited[splited.length - 1];
  let filepath = RNFS.DocumentDirectoryPath + `/${filename}`;
  let absolutePath = 'file://' + filepath;
  if (await RNFS.exists(filepath)) {
    return {exist: true, path: absolutePath};
  } else {
    return {exist: false, path: absolutePath};
  }
};

const DownloadButton = props => {
  const [disabled, setDisabled] = React.useState(false);
  const handleDownload = () => {
    setDisabled(true);
    let splited = props.url.split('/');
    let filename = splited[splited.length - 1];
    download({
      url: props.url,
      filename: filename,
      progressCallback: props.progress,
    }).then(() => {
      if (props.onFinish) {
        props.onFinish();
      }
    });
  };

  return (
    <Button
      title="Download"
      icon={{
        name: 'download',
        type: 'font-awesome',
        size: 26,
        color: 'white',
      }}
      disabled={disabled}
      onPress={handleDownload}
    />
  );
};

export {DownloadButton, isDownloaded};
