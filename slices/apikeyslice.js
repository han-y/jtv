import {createSlice} from '@reduxjs/toolkit';
export default APIKEYSlice = createSlice({
  name: 'APIKEY',
  initialState: {
    value: '',
  },
  reducers: {
    setAPIKEY(state, action) {
      const key = action.payload;
      state.value = key;
    },
  },
});

export const {setAPIKEY} = APIKEYSlice.actions;
