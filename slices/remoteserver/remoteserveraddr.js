import {createSlice} from '@reduxjs/toolkit';

export default RemoteServerAddrSlice = createSlice({
  name: 'remoteServerAddr',
  initialState: {
    value: 'https://api.hanyoung.uk',
  },
  reducers: {
    setRemoteServerAddr(state, action) {
      const key = action.payload;
      state.value = key;
    },
  },
});

export const {setRemoteServerAddr} = RemoteServerAddrSlice.actions;