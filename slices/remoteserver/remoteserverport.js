import {createSlice} from '@reduxjs/toolkit';
export default RemoteServerPortSlice = createSlice({
  name: 'remoteServerPort',
  initialState: {
    value: '443',
  },
  reducers: {
    setRemoteServerPort(state, action) {
      const key = action.payload;
      state.value = key;
    },
  },
});

export const {setRemoteServerPort} = RemoteServerPortSlice.actions;
