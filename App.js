import React from 'react';
import {useColorScheme} from 'react-native';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import store from './store';
import {Settings} from './settingspage';
import {HomePage} from './homepage';
import {LocalVideoView} from './localvideoview';
import {FamilyMedia} from './family';
import InitApp from "./initsettings";

InitApp();

const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={HomePage} />
      <Drawer.Screen name="FamilyMedia" component={FamilyMedia} />
      <Drawer.Screen name="LocalVideos" component={LocalVideoView} />
      <Drawer.Screen name="Setting" component={Settings} />
    </Drawer.Navigator>
  );
}

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <Provider store={store}>
      <NavigationContainer>
        <MyDrawer />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
