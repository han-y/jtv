import {configureStore} from '@reduxjs/toolkit';
import APIKEYSlice from './slices/apikeyslice';
import RemoteServerAddrSlice from './slices/remoteserver/remoteserveraddr';
import RemoteServerPortSlice from './slices/remoteserver/remoteserverport';

export default configureStore({
  reducer: {
    APIKEY: APIKEYSlice.reducer,
    RemoteServerAddr: RemoteServerAddrSlice.reducer,
    RemoteServerPort: RemoteServerPortSlice.reducer,
  },
});
