import React from 'react';
import {Text} from 'react-native-elements';
const PlaceHolderText = ({text}) => {
  return (
    <Text
      style={{
        flex: 1,
        color: 'grey',
        fontSize: 18,
        textAlignVertical: 'center',
        textAlign: 'center',
      }}>
      {text}
    </Text>
  );
};
export default PlaceHolderText;
