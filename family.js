import React from 'react';
import {FlatList} from 'react-native';
import {ListItem, Icon, Avatar, Text, Button} from 'react-native-elements';
import {createStackNavigator} from '@react-navigation/stack';
import Video from 'react-native-video';
import GallerySwiper from 'react-native-gallery-swiper';
import LinearGradient from 'react-native-linear-gradient';
import {useLocalServerAddr} from './findlocalserver';
import {DownloadButton, isDownloaded} from './downloader';
import PlaceHolder from './utility/placeholdertext';
const Stack = createStackNavigator();

const FamilyMedia = ({navigation, route}) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        style={{flex: 1}}
        name="TopLevelDirectoryView"
        component={TopLevelDirectoryView}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="DirectoryView"
        component={DirectoryView}
        options={({route}) => ({title: route.params.name})}
      />
      <Stack.Screen
        name="ImageView"
        component={ImageView}
        options={({route}) => ({title: route.params.name})}
      />
      <Stack.Screen
        name="VideoView"
        component={VideoView}
        options={({route}) => ({title: route.params.name})}
      />
    </Stack.Navigator>
  );
};

const TopLevelDirectoryView = ({navigation, route}) => {
  const url = useLocalServerAddr();
  if (!url) {
    return <PlaceHolder text="Can't Find Server in Local Network" />;
  }
  route.params = {url: url + '/family'};
  return <DirectoryView navigation={navigation} route={route} />;
};

let directoryViewTimerId = null;
const DirectoryView = ({navigation, route}) => {
  const [rootData, setRootData] = React.useState([]);
  const [requestFailed, setRequestFailed] = React.useState(false);

  React.useEffect(() => {
    if (directoryViewTimerId) {
      clearInterval(directoryViewTimerId);
      directoryViewTimerId = null;
    }

    const fetchData = async () => {
      if (!route.params.url) {
        return;
      }
      try {
        let data = await (await fetch(route.params.url)).json();
        setRootData(data);
        setRequestFailed(false);
        if (directoryViewTimerId) {
          clearInterval(directoryViewTimerId);
          directoryViewTimerId = null;
        }
      } catch (e) {
        setRequestFailed(true);
        console.warn(e, 'url: ', route.params.url);
        if (!directoryViewTimerId) {
          directoryViewTimerId = setInterval(fetchData, 1000);
        }
      }
    };

    fetchData();
  }, [route.params.url]);
  const renderItem = ({item}) => {
    let fileObject;
    if (item.file_type === 'dir') {
      fileObject = (
        <Directory
          url={item.url}
          name={item.file_name}
          navigation={navigation}
        />
      );
    } else if (item.file_type === 'image') {
      fileObject = (
        <ImageItem
          url={item.url}
          data={rootData}
          name={item.file_name}
          navigation={navigation}
        />
      );
    } else if (item.file_type === 'video') {
      fileObject = (
        <VideoItem
          url={item.url}
          name={item.file_name}
          navigation={navigation}
        />
      );
    }
    return fileObject;
  };

  if (requestFailed) {
    return (
      <Text
        style={{
          flex: 1,
          color: 'grey',
          fontSize: 18,
          textAlignVertical: 'center',
          textAlign: 'center',
        }}>
        The Local Server Doesn't Seen to Respond
      </Text>
    );
  } else if (rootData.length === 0) {
    return <PlaceHolder text="Directory is Empty" />;
  } else {
    return (
      <FlatList
        style={{flex: 1}}
        data={rootData}
        renderItem={renderItem}
        keyExtractor={item => item.url}
      />
    );
  }
};

const Directory = props => {
  const navigateTo = () => {
    props.navigation.push('DirectoryView', {url: props.url, name: props.name});
  };
  return (
    <ListItem key={props.url} bottomDivider onPress={navigateTo}>
      <Icon name="folder" />
      <ListItem.Content>
        <ListItem.Title>{props.name}</ListItem.Title>
      </ListItem.Content>
    </ListItem>
  );
};

const ImageItem = props => {
  const [rearButton, setRearButton] = React.useState(null);
  const navigateTo = () => {
    let index = props.data.findIndex(({url}) => url === props.url);
    props.navigation.push('ImageView', {
      data: props.data,
      name: props.name,
      initialPage: index,
    });
  };
  React.useEffect(() => {
    const checkExist = async () => {
      let downloaded = await isDownloaded(props.url);

      let downloadedButton = <Button title="Open" onPress={navigateTo} />;
      if (!downloaded.exist) {
        setRearButton(
          <DownloadButton
            style={{alignSelf: 'flex-end', flex: 1}}
            url={props.url}
            onFinish={() => {
              setRearButton(downloadedButton);
            }}
          />,
        );
      } else {
        setRearButton(downloadedButton);
      }
    };
    checkExist();
  }, []);

  return (
    <ListItem key={props.url} bottomDivider onPress={navigateTo}>
      <Avatar size="xlarge" rounded source={{uri: props.url}} />
      <ListItem.Content
        style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}>
        <ListItem.Title style={{flex: 1}}>{props.name}</ListItem.Title>
        {rearButton}
      </ListItem.Content>
    </ListItem>
  );
};

const ImageView = ({navigation, route}) => {
  let images = route.params.data;

  return (
    <GallerySwiper images={images} initialPage={route.params.initialPage} />
  );
};

const VideoItem = props => {
  const [rearButton, setRearButton] = React.useState(null);
  const [progress, setProgress] = React.useState(0.001);

  const updateDownloadProgress = ({contentLength, bytesWritten}) => {
    setProgress(bytesWritten/contentLength);
  };
  const navigateTo = () => {
    props.navigation.push('VideoView', {url: props.url, name: props.name});
  };
  React.useEffect(() => {
    const checkExist = async () => {
      let downloaded = await isDownloaded(props.url);
      let downloadedButton = (
        <Button
          title="Open"
          onPress={() => {
            props.navigation.push('VideoView', {
              url: downloaded.path,
              name: props.name,
            });
          }}
        />
      );
      if (!downloaded.exist) {
        setRearButton(
          <DownloadButton
            style={{alignSelf: 'flex-end', flex: 1}}
            url={props.url}
            onFinish={() => {
              setRearButton(downloadedButton);
              setProgress(0.001);
            }}
            progress={updateDownloadProgress}
          />,
        );
      } else {
        setRearButton(downloadedButton);
      }
    };
    checkExist();
  }, []);
  return (
    <ListItem
      key={props.url}
      bottomDivider
      onPress={navigateTo}
      linearGradientProps={{
        colors: ['transparent', '#1abc4b'],
        start: {x: progress, y: 0},
        end: {x: 0, y: 0},
        locations: [0,0]
      }}
      ViewComponent={LinearGradient}>
      <Icon name="ondemand-video" />
      <ListItem.Content
        style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}>
        <ListItem.Title style={{flex: 1}}>{props.name}</ListItem.Title>
        {rearButton}
      </ListItem.Content>
    </ListItem>
  );
};

const VideoView = ({navigation, route}) => {
  let url = route.params.url;
  return (
    <Video
      resizeMode={'contain'}
      controls={true}
      style={{flex: 1}}
      source={{uri: url}}
    />
  );
};
export {FamilyMedia};
