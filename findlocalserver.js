import React from 'react';
import dgram from 'react-native-udp';
import NetInfo from '@react-native-community/netinfo';
const socket = dgram.createSocket('udp4');
bindSocket = (number) => socket.bind(number, err => {
  if (err) {
    bindSocket(number + 1);
  }
});
bindSocket(10243);
let timerId = null;
const useLocalServerAddr = () => {
  const [addr, setAddr] = React.useState('');

  React.useEffect(() => {
    const send = () => {
      socket.send(
        'JTV LOCAL SERVER REQUEST',
        undefined,
        undefined,
        37820,
        '255.255.255.255',
        function (err) {
          if (err) console.log(err);
        },
      );
    };
    const unsubscribe = NetInfo.addEventListener(state => {
      send();
    });
    socket.once('listening', function () {
      send();
    });
    timerId = setInterval(() => {
        send();
      }, 3000);

    socket.on('message', function (msg, rinfo) {
      if (msg.toString() === 'JTV LOCAL SERVER ACK') {
        setAddr(`http://${rinfo.address}`);
        if (timerId) {
          clearInterval(timerId);
        }
      }
    });
  }, []);

  return addr;
};

export {useLocalServerAddr};
